# -*- coding: utf-8 -*-
from collections import deque
from glob import iglob
from pathlib import Path
from tkinter.filedialog import askdirectory
from typing import TypeAlias

StrPath: TypeAlias = str | Path


class DirectoryFile:
    def __init__(self, file: Path):
        self._file: Path = file
        self._content: list[str] = []

    def read(self):
        with open(self._file, "r") as f:
            _: list[str] = f.readlines()
        self._content = _

    def write(self):
        with open(self._file, "w") as f:
            f.write("\n".join(self._content))

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        if isinstance(item, int):
            return self._content[item]
        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, int) and isinstance(value, str):
            self._content[key] = value
        else:
            raise TypeError

    @property
    def content(self):
        return self._content


class Replacer:
    def __init__(self, root_dir: StrPath, dir_file: DirectoryFile | None = None):
        self._root_dir: Path = Path(root_dir)
        self._dir_file: DirectoryFile | None = dir_file
        self._dict_names: dict[str, str] = dict()
        self._changed: bool = False
        self._log_lines: deque[str] = deque()

    @property
    def dir_file(self):
        return self._dir_file

    @dir_file.setter
    def dir_file(self, value):
        self._dir_file = value

    def set_dict_names(self):
        _basic_name: str = self._root_dir.name.split("_")[-1]
        _dict_names: dict[str, str] = {
            "%ProjectName%": f"PROTEI_{_basic_name.upper()}",
            "%ShortProjectName": f"{_basic_name}",
            "%ProjectDir%": f"Protei_{_basic_name}",
            "%ServiceName%": f"{_basic_name.lower()}"
        }
        self._dict_names.update(**_dict_names)

    def __bool__(self):
        return self._changed

    def multiple_replace(self, index: int, line: str):
        for k, v in self._dict_names.items():
            if k in line:
                self._log_lines.append(f"В строке {index} замена {k} -> {v}")
                line = line.replace(k, v)
        return line

    def output_logs(self):
        if bool(self):
            self._log_lines.appendleft(f"========================================")
            self._log_lines.appendleft(f"Файл {self._dir_file} изменен")
            self._log_lines.append("========================================")
            self._log_lines.append("\n")

            for log_line in self._log_lines:
                print(log_line)

    def clear(self):
        self._dir_file = None
        self._log_lines = []


def get_files(root_dir: StrPath) -> list[Path]:
    return [
        root_dir.joinpath(file)
        for file in iglob("**/*", root_dir=root_dir, recursive=True)]


def find_initial_dir() -> Path:
    _dirs: tuple[str, ...] = ("PycharmProjects", "IdeaProjects")
    for _dir in _dirs:
        _: Path = Path.home().joinpath(_dir)
        if _.exists():
            return _
    else:
        return Path.home().joinpath("Desktop")


def get_user_input() -> str:
    initialdir: Path = find_initial_dir()
    # noinspection SpellCheckingInspection
    mustexist: bool = True
    title: str = "Выберите корень проекта"
    return askdirectory(initialdir=initialdir, mustexist=mustexist, title=title)


def main():
    dirpath: str = get_user_input()

    replacer: Replacer = Replacer(dirpath)
    replacer.set_dict_names()

    for file in get_files(dirpath):
        dir_file: DirectoryFile = DirectoryFile(file)
        dir_file.read()
        replacer.dir_file = dir_file

        for index, line in enumerate(iter(dir_file)):
            dir_file[index] = replacer.multiple_replace(index, line)

        if bool(replacer):
            dir_file.write()
            replacer.output_logs()

        replacer.clear()


if __name__ == '__main__':
    main()
