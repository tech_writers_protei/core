from typing import Iterable, NamedTuple


class Line(NamedTuple):
    name: str
    value: str

    def __str__(self):
        return f"{self.name} = {self.value}"

    def __len__(self):
        return len(self.name)

    def formatting(self, shift: int = 1):
        return f"{self.name}"


def format_lines(lines: Iterable[str] = None):
    if lines is None:
        return
    else:
        lines: list[str] = [*lines]

    _short_lines: list[Line] = []

    for line in lines:
        name, value = line.split(" = ")
        short_line: Line = Line(name, value)
        _short_lines.append(short_line)

    max_length: int = max(len(item) for item in _short_lines)
    _formatted: list[str] = [f"{short_line.name:{max_length}} = {short_line.value}," for short_line in _short_lines]
    # _formatted: list[str] = [(":{len}" % max_length).format(short_line) for short_line in _short_lines]

    return _formatted


if __name__ == '__main__':
    _lines = [
        "e_100_Continue = 100",
        "e_101_Switching_Protocols = 101",
        "e_102_Processing = 102",
        "e_103_Early_Hints = 103",
        "e_200_OK = 200",
        "e_201_Created = 201",
        "e_202_Accepted = 202",
        "e_203_Non_Authoritative_Information = 203",
        "e_204_No_Content = 204",
        "e_205_Reset_Content = 205",
        "e_206_Partial_Content = 206",
        "e_207_Multi_Status = 207",
        "e_208_Already_Reported = 208",
        "e_226_IM_Used = 226",
        "e_300_Multiple_Choices = 300",
        "e_301_Moved_Permanently = 301",
        "e_302_Found = 302",
        "e_303_See_Other = 303",
        "e_304_Not_Modified = 304",
        "e_305_Use_Proxy = 305",
        "e_306_Switch_Proxy = 306",
        "e_307_Temporary_Redirect = 307",
        "e_308_Permanent_Redirect = 308",
        "e_400_Bad_Request = 400",
        "e_401_Unauthorized = 401",
        "e_402_Payment_Required = 402",
        "e_403_Forbidden = 403",
        "e_404_Not_Found = 404",
        "e_405_Method_Not_Allowed = 405",
        "e_406_Not_Acceptable = 406",
        "e_407_Proxy_Authentication_Required = 407",
        "e_408_Request_Time_out = 408",
        "e_409_Conflict = 409",
        "e_410_Gone = 410",
        "e_411_Length_Required = 411",
        "e_412_Precondition_Failed = 412",
        "e_413_Request_Entity_Too_Large = 413",
        "e_414_Request_URI_Too_Large = 414",
        "e_415_Unsupported_Media_Type = 415",
        "e_416_Requested_range_not_satisfiable = 416",
        "e_417_Expectation_Failed = 417",
        "e_418_I_am_a_teapot = 418",
        "e_421_Misdirected_Request = 421",
        "e_422_Unprocessable_Entity = 422",
        "e_423_Locked = 423",
        "e_424_Failed_Dependency = 424",
        "e_425_Too_Early = 425",
        "e_426_Upgrade_Required = 426",
        "e_428_Precondition_Required = 428",
        "e_429_Too_Many_Requests = 429",
        "e_431_Request_Header_Fields_Too_Large = 431",
        "e_451_Unavailable_For_Legal_Reasons = 451",
        "e_500_Internal_Server_Error = 500",
        "e_501_Not_Implemented = 501",
        "e_502_Bad_Gateway = 502",
        "e_503_Service_Unavailable = 503",
        "e_504_Gateway_Time_out = 504",
        "e_505_HTTP_Version_not_supported = 505",
        "e_506_Variant_Also_Negotiates = 506",
        "e_507_Insufficient_Storage = 507",
        "e_508_Loop_Detected = 508",
        "e_510_Not_Extended = 510",
        "e_511_Network_Authentication_Required = 511"
    ]

    for _ in format_lines(_lines):
        print(_)
