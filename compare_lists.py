from typing import Iterable


def compare_iterables(first: Iterable[str] = None, second: Iterable[str] = None):
    if first is None and second is None:
        return list()
    elif first is None:
        return sorted(*second)
    elif second is None:
        return sorted(*first)
    else:
        return sorted(set.symmetric_difference({*first}, second))



def parse_command_line():
    first = set(iter(input, ""))
    second = set(iter(input, ""))
    # first = input("Input first iterable:\n").splitlines()
    # second = input("Input second iterable:\n").splitlines()

    print(compare_iterables(first, second))


if __name__ == '__main__':
    parse_command_line()
