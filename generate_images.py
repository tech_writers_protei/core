# -*- coding: utf-8 -*-
from pathlib import Path
from string import hexdigits
from typing import Iterable, NamedTuple
# noinspection StandardLibraryXml
from xml.etree.ElementTree import ElementTree, Element, tostring, parse, register_namespace

from cairosvg import svg2png
from loguru import logger


class SVGImageXMLHandler:
    def __init__(self):
        self._path: Path | None = None
        self._element_tree: ElementTree | None = None
        self._element: Element | None = None
        register_namespace("", "http://www.w3.org/2000/svg")

    def __str__(self):
        return tostring(self._element, encoding="unicode", xml_declaration=False)

    def __bytes__(self):
        return tostring(self._element, encoding="utf-8", xml_declaration=False)

    def __bool__(self):
        return self._element_tree is not None

    def read(self, path: str | Path):
        self._path = path
        self._element_tree = parse(path)
        self._element = self._element_tree.getroot()

    def nullify(self):
        self._path = None
        self._element_tree = None
        self._element = None

    def __getitem__(self, item):
        if isinstance(item, str):
            return self._element.get(item, None)
        else:
            raise KeyError

    def __setitem__(self, key, value):
        if isinstance(key, str) and hasattr(value, "__str__"):
            self._element.set(key, str(value))
        else:
            raise TypeError

    def set_param(self, key: str, value: str):
        if self[key] is not None:
            self[key] = value
        else:
            print(f"Элемент не имеет атрибута {key}")

    def write(self):
        with open(self._path, "wb") as fb:
            fb.write(bytes(self))
            print(f"Файл {self._path} обновлен")


svg_image_xml_handler: SVGImageXMLHandler = SVGImageXMLHandler()


class Color(NamedTuple):
    name: str
    hex_code: str

    def __repr__(self):
        return f"Color {self.name}: {self.hex_code}"

    def __str__(self):
        return f"{self.hex_code}"


class ColorDict(dict):
    def __repr__(self):
        return f"<{self.__class__.__name__}>"

    __str__ = __repr__

    def all(self):
        return "\n".join(repr(Color(name, hex_code)) for name, hex_code in self.items())

    def register_color(self, color: Color):
        if color.name in self.keys():
            logger.error(f"Color {color.name} is already registered")
            return

        elif len(color.hex_code) > 6 or len(color.hex_code) < 3:
            logger.error(f"Color {color.name} cannot have a {color.hex_code} hex code")
            return

        elif any(symbol not in hexdigits for symbol in color.hex_code):
            logger.error(f"Color {color.name} has an invalid {color.hex_code} hex code")
            return

        else:
            self[color.name] = color.hex_code.upper()

    def __add__(self, other):
        if isinstance(other, Color):
            self.register_color(other)
        elif isinstance(other, Iterable):
            colors: tuple[Color, ...] = tuple(filter(lambda x: isinstance(x, Color), other))
            for color in colors:
                self.register_color(color)
        else:
            logger.error(f"Invalid item {other} of type {type(other)} to add")
            raise ValueError


black: Color = Color("black", "#000")
white: Color = Color("white", "#FFF")
gray_60: Color = Color("gray", "#7D858F")
red_80: Color = Color("red", "#EB4B69")
green_90: Color = Color("green", "#3DB670")
light_blue: Color = Color("light blue", "#5D9AF4")
bs_body_color: Color = Color("bs-body-color", "#212529")
green_20: Color = Color("light green", "#D4EFDF")
green_100: Color = Color("dark green", "#27AE60")
red_10: Color = Color("light red", "#FCE9EC")
red_100: Color = Color("dark blue", "#E61E43")
yellow_10: Color = Color("light yellow", "#FFFAEA")
yellow_100: Color = Color("yellow", "#FCC929")

color_dict: ColorDict = ColorDict()
color_dict + (
    black,
    white,
    gray_60,
    red_80,
    green_90,
    light_blue,
    bs_body_color,
    green_20,
    green_100,
    red_10,
    red_100,
    yellow_10,
    yellow_100)


class ImageConverter:
    def __init__(
            self,
            path: Path | None = None,
            width: int | None = None,
            height: int | None = None,
            color: str | None = None,
            thickness: int | None = None,
            background: str | None = None,
            scale: int | None = None,
            output: str | None = None):
        self._path: Path | None = path
        self._width: int | None = width
        self._height: int | None = height
        self._color: str | None = color
        self._thickness: int | None = thickness
        self._background: str | None = background
        self._scale: int | None = scale
        self._output: str | Path | None = output

    def normalize(self):
        if self._scale is not None:
            self._width = None
            self._height = None

        if (self._scale, self._width, self._height) is (None, None, None):
            self._scale = 1

        if self._output is None:
            self._output: Path = self._path.with_suffix(".png")

        if not self._color.startswith("#"):
            self._color = color_dict.get(self._color)

    @property
    def path(self):
        return self._path

    @path.setter
    def path(self, value):
        self._path = value

    @property
    def width(self):
        return self._width

    @width.setter
    def width(self, value):
        self._width = value

    @property
    def height(self):
        return self._height

    @height.setter
    def height(self, value):
        self._height = value

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, value):
        self._color = value

    @property
    def thickness(self):
        return self._thickness

    @thickness.setter
    def thickness(self, value):
        self._thickness = value

    @property
    def background(self):
        return self._background

    @background.setter
    def background(self, value):
        self._background = value

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = value

    @property
    def output(self):
        return self._output

    @output.setter
    def output(self, value):
        self._output = value

    def __bool__(self):
        return self._path is not None

    def specify_color(self):
        if self._color is not None:
            svg_image_xml_handler.read(self._path)
            svg_image_xml_handler.set_param("stroke", f"{self._color}")
            svg_image_xml_handler.write()
            svg_image_xml_handler.nullify()
            print(f"Для файла {self._path.name} задан цвет {self._color}")

    def specify_thickness(self):
        if self._thickness is not None:
            svg_image_xml_handler.read(self._path)
            svg_image_xml_handler.set_param("stroke-width", f"{self._thickness}")
            svg_image_xml_handler.write()
            svg_image_xml_handler.nullify()
            print(f"Для файла {self._path.name} задана толщина {self._thickness}")

    def convert(self):
        if not bool(self):
            logger.error(f"No file is specified")
            raise ValueError

        self.specify_color()
        self.specify_thickness()

        with open(self._path, "rb") as file_obj:
            svg2png(
                file_obj=file_obj,
                scale=self._scale,
                background_color=self._background,
                output_width=self._width,
                output_height=self._height,
                write_to=self._output)
