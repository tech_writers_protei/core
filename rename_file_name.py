# -*- coding: utf-8 -*-
from glob import iglob
from os import sep
from pathlib import Path


def rename(pathdir: str):
    for file_path in iglob("**/*", root_dir=pathdir, recursive=True):
        path: Path = Path(f"{pathdir}{sep}{file_path}")
        if path.suffix != path.suffix.lower():
            path.rename(path.with_suffix(path.suffix.lower()))


if __name__ == '__main__':
    path: str = r"C:\Users\tarasov-a\PycharmProjects\Globus_PASS\content\common\web\sections\images"
    rename(path)
