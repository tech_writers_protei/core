from glob import iglob


def get_all_files(path: str):
    for item in iglob("*.adoc", root_dir=path):
        print(f"include::ready/{item}[]\n")


def generate_temp_file(path: str):
    with open(path, "w", encoding="utf-8") as f:
        for i in range(1_000_000):
            f.write(f"{str(i)}\n")
    print(f"Success. {path}")


if __name__ == '__main__':
    path: str = r"C:\Users\tarasov-a\Desktop\text.txt"
    generate_temp_file(path)
