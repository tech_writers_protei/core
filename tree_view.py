# -*- coding: utf-8 -*-
from argparse import ArgumentParser, SUPPRESS, Namespace, ArgumentError
from pathlib import Path

from rich import print
from rich.filesize import decimal
from rich.markup import escape
from rich.text import Text
from rich.tree import Tree

PRESS_ENTER_KEY: str = "\nНажмите ENTER, чтобы завершить работу скрипта ..."


def walk_directory(directory: Path, tree: Tree) -> None:
    """Recursively build a Tree with directory contents."""
    # Sort dirs first then by filename
    paths: list[Path] = sorted(
        Path(directory).iterdir(),
        key=lambda x: (x.is_file(), x.name.lower()),
    )
    for path in paths:
        if path.is_dir():
            style: str = "dim" if path.name.startswith("__") else ""
            branch: Tree = tree.add(
                f"[bold magenta]:open_file_folder: [link file://{path}]{escape(path.name)}/",
                style=style,
                guide_style=style,
            )
            walk_directory(path, branch)

        else:
            text_filename: Text = Text(path.name, "green")
            text_filename.highlight_regex(r"\..*$", "bold red")
            text_filename.stylize(f"link file://{path}")
            # file_size: int = path.stat().st_size
            # text_filename.append(f" ({decimal(file_size)})", "blue")
            tree.add(text_filename)


if __name__ == '__main__':
    arg_parser: ArgumentParser = ArgumentParser(
        prog="tree",
        usage=f"py {Path(__file__).name} [ -h/--help | -v/--version ] <DIR>",
        description="Вывести содержимое папки в виде дерева",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        "directory",
        type=str,
        action="store",
        default=".")

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Показать версию скрипта и завершить работу"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу"
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(PRESS_ENTER_KEY)
            exit(0)

    except ArgumentError as e:
        print(f"{e.__class__.__name__}, {e.argument_name}\n{e.message}")
        input(PRESS_ENTER_KEY)
        exit(1)

    except KeyboardInterrupt:
        print("Ошибка -1")
        print("Работа скрипта остановлена пользователем ...")
        exit(-1)

    except IndexError:
        print("[b]Usage:[/] python tree.py <DIRECTORY>")

    else:
        directory: str = getattr(args, "directory")
        tree: Tree = Tree(
            f":open_file_folder: [link file://{directory}]{directory}",
            guide_style="bold bright_blue",
        )
        walk_directory(Path(directory), tree)
        print(tree)
