def to_minutes(value: str):
    h, m = value.split(":", 1)
    return 60 * int(h) + int(m)


def summarize(*values: str):
    return sum(map(to_minutes, values))


def convert_to_hours(line: str):
    if ";" not in line:
        return line

    minutes: str = line.rsplit(";", 1)[-1]
    h, m = divmod(int(minutes), 60)
    if not m:
        _str: str = f"{h} ч"
    else:
        _str: str = f"{h} ч {m} м"

    return line.replace(minutes, _str)


def convert_in_file(path: str):
    with open(path, "r") as f:
        _content: list[str] = f.readlines()

    for index, line in enumerate(_content):
        _content[index] = convert_to_hours(line)

    with open(path, "w") as f:
        f.write("\n".join(_content))

    print("Success")


if __name__ == '__main__':
    # _values: tuple[str, ...] = (
    #     "38:20", "32:32", "36:01", "32:36", "36:39", "30:00", "37:24", "36:34", "32:51", "26:28")
    # _required: tuple[int, ...] = (40, 32, 40, 32, 40, 32, 40, 40, 40, 32)
    # print(summarize(*_values))
    # print(sum(_required) * 60)
    # print(f"--------------------")
    # print(divmod(summarize(*_values), 60))
    # print(sum(_required))
    # print(f"--------------------")
    # print(sum(_required) * 60 - summarize(*_values))
    # print(divmod(sum(_required) * 60 - summarize(*_values), 60))
    path: str = "/Tarasov_report_jan_feb_mar_2024.csv"
    convert_in_file(path)
