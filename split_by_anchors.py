from pathlib import Path


class AdocFile:
    def __init__(self, path: str):
        self._path: Path = Path(path).resolve()
        self._content: list[str] = []
        self._indexes: list[int] = []

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        return self._content[item]

    def read(self):
        with open(self._path, "r") as f:
            _: list[str] = f.readlines()
        self._content = _

    def find_anchors(self):
        self._indexes = [index for index, line in enumerate(iter(self)) if "[[monitoring" in line and "]]" in line]
        self._indexes.append(-1)

    def get_part(self, part: int) -> list[str]:
        return [self[index] for index in range(self._indexes[part], self._indexes[part + 1])]

    @property
    def file_names(self) -> list[str]:
        return [self[index].removeprefix("[[").removesuffix("]]\n").replace("-", "_") for index in self._indexes]

    @property
    def len_indexes(self):
        return len(self._indexes) - 1

    def process(self):
        for index in range(self.len_indexes):
            _content: list[str] = self.get_part(index)
            file_name: str = self.file_names[index]

            with open(self._path.with_stem(file_name), "w") as f:
                f.write("".join(_content))


if __name__ == '__main__':
    adoc_file: AdocFile = AdocFile(r"C:\Users\tarasov-a\PycharmProjects\Globus_OSS\content\common\oam\api\monitoring.adoc")
    adoc_file.read()
    adoc_file.find_anchors()
    adoc_file.process()
