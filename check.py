import string
from glob import iglob
from pathlib import Path


def get_files(path: str):
    path: Path = Path(path)
    for item in iglob("*/**", root_dir=path, recursive=True):
        if (_ := Path(path).joinpath(item)).is_dir():
            print(f"\n{_.stem.capitalize()}:")
        elif Path(path).joinpath(item).suffix in (".png", ".svg"):
            continue
        elif (_ := Path(path).joinpath(item)).is_file():
            _item: str = item.replace('\\', '/')
            print(f"- content/common/{_item}")



def get_symbols():
    print(f"{string.digits}{string.ascii_letters}#*")



if __name__ == '__main__':
    # _path = r"C:\Users\tarasov-a\PycharmProjects\Globus_OSS\content\common"
    # get_files(_path)
    get_symbols()

#
# "monitoring/alerts/updateStatus"
# "monitoring/hierarchy/group/create"
# "monitoring/objects/dashboards/change"
# "monitoring/objects/terminal/{Generated-CLI-WS-Token}"
# "monitoring/objects/{Object-Id}/configuration/{api-method}"
