# -*- coding: utf-8 -*-
from typing import NamedTuple, TypeAlias, Literal

BracketType: TypeAlias = Literal["open", "close"]


class BracketCounter:
    def __init__(self, line: str):
        self._line: str = line
        self._bracket_level: int = 0
        self._brackets: list[Bracket] = []

    def __iter__(self):
        return iter(self._line)

    def __bool__(self):
        return self._bracket_level == 0

    def get_brackets(self):
        for char_index, char in enumerate(iter(self)):
            if char == "(":
                char_type: BracketType = "open"
                level: str = f"{self._bracket_level}_{self._bracket_level + 1}"
            elif char==")":
                char_type: BracketType = "close"
                level: str = f"{self._bracket_level}_{self._bracket_level - 1}"

                self._brackets.append(Bracket(char_index, char_type, level))


class Bracket(NamedTuple):
    char_index: int
    char_type: BracketType
    level: str

    @property
    def _split(self) -> list[str]:
        return self.level.split("_", 1)

    def direct(self) -> tuple[int, ...]:
        return int(self._split[0]), int(self._split[1])

    def transpose(self) -> tuple[int, ...]:
        return int(self._split[1]), int(self._split[0])

    def is_paired(self, other):
        if not isinstance(other, self.__class__):
            return NotImplemented
        if self.char_type == other.char_type:
            return False
        if self.transpose() == other.direct():
            return True


def paired_brackets(bracket_one: Bracket, bracket_two: Bracket):
    if bracket_one.char_type == bracket_two.char_type:
        return False


def longest(a1, a2):
    return "".join(sorted(set().union(a1).union(a2)))


if __name__ == '__main__':
    line_1, line_2 = "loopingisfunbutdangerous", "lessdangerousthancoding"
    line_3 = "loopingisfunbutdangerous"
    print(longest(line_1, line_2))
    print(longest(line_3, line_3))
