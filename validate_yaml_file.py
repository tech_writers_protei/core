# -*- coding: utf-8 -*-
from argparse import ArgumentError, ArgumentParser, Namespace, OPTIONAL, SUPPRESS
from os import scandir
import os.path as p

_FAIL_ = '\33[41m'
_PASS_ = '\33[42m'

_DICT_RESULTS: dict[bool, dict[str, str]] = {
    True: {
        "status": "OK",
        "color": _PASS_
    },
    False: {
        "status": "NOT OK",
        "color": _FAIL_
    }
}

STOP: str = "Нажмите любую клавишу для завершения скрипта ...\n"
NAME: str = p.basename(__file__)


def check_path(path: str):
    if not path:
        input(STOP)
        exit(3)

    abs_path: str = p.expanduser(path)

    if not p.exists(abs_path):
        input("Указан путь до не существующего файла")
        exit(4)

    return abs_path


def parse():
    arg_parser: ArgumentParser = ArgumentParser(
        prog="validate_yaml_file",
        usage=f"py {NAME} <YAML_FILE> [ -h/--help | -v/--version ]",
        description="Валидация YAML-файла для PDF",
        epilog="",
        add_help=False,
        allow_abbrev=False,
        exit_on_error=False
    )

    arg_parser.add_argument(
        "yaml_file",
        action="store",
        nargs=OPTIONAL,
        type=str,
        default=None,
        help="Путь до файла PDF_*.yml"
    )

    arg_parser.add_argument(
        "-o", "--output",
        action="store",
        default=SUPPRESS,
        required=False,
        help="Файл для записи вывода",
        dest="output"
    )

    arg_parser.add_argument(
        "-v", "--version",
        action="version",
        version="1.0.0",
        help="Показать версию скрипта и завершить работу"
    )

    arg_parser.add_argument(
        "-h", "--help",
        action="help",
        default=SUPPRESS,
        help="Показать справку и завершить работу"
    )

    try:
        args: Namespace = arg_parser.parse_args()

        if hasattr(args, "help") or hasattr(args, "version"):
            input(STOP)
            return

    except ArgumentError as exc:
        print(f"{exc.__class__.__name__}, {exc.argument_name}\n{exc.message}")
        input(STOP)
        exit(-1)

    except KeyboardInterrupt:
        input("Работа скрипта остановлена пользователем ...")
        exit(-2)

    else:
        return args


def get_value(namespace: Namespace, attribute: str):
    return getattr(namespace, attribute, None)


def validate(yaml_file: str | None):
    if yaml_file is None:
        for file in scandir(p.curdir):
            name, extension = p.splitext(file.name)

            if extension == ".yml" and name.startswith("PDF"):
                yaml_file = file.name
                print(f"Обнаружен файл {yaml_file}\n")
                break

        else:
            print("Не найден YAML-файл для чтения")
            user_input: str = input("Укажите путь до файла:\n\n")

            yaml_file = check_path(user_input)

    with open(yaml_file, "r") as f:
        paths: list[str] = [
            p.join(p.dirname(yaml_file), line.strip().removeprefix("- "))
            for line in f.readlines()
            if line.strip().startswith("-")]

    max_length: int = max(map(len, paths)) + 10
    _lines: list[str] = []

    for path in paths:
        _result: bool = p.exists(path)
        _status: str = _DICT_RESULTS.get(_result).get("status")
        _color: str = _DICT_RESULTS.get(_result).get("color")

        _line = f"{_color}{path:.<{max_length}}{_status:.>6}"
        _lines.append(_line)

    return "\n".join(_lines)


def write_to_file(content: str, path: str | None = None):
    if path is None or not path:
        return
    else:
        with open(path, "w") as f:
            f.write(content.replace(_PASS_, "").replace(_FAIL_, ""))
        return


if __name__ == '__main__':
    ns: Namespace = parse()
    yaml: str = get_value(ns, "yaml_file")
    result: str = validate(yaml)
    print(result)
    output: str | None = get_value(ns, "output")
    write_to_file(result, output)
