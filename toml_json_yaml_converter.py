# -*- coding: utf-8 -*-
from enum import Enum
from json.decoder import JSONDecodeError
from pathlib import Path
from tomlkit.exceptions import ConvertError, ParseError as TOMLDecodeError
from typing import Any, Mapping

from yaml.error import MarkedYAMLError, YAMLError


class FileType(Enum):
    JSON = ".json"
    TOML = ".toml"
    YAML = ".yaml"



def get_file_content(file: str | Path):
    try:
        with open(file, "r", encoding="utf-8", errors="ignore") as f:
            match file.suffix:
                case ".json":
                    from json import load

                case ".toml":
                    from tomlkit import load

                case ".yaml":
                    from yaml import safe_load as load

                case _:
                    raise ValueError

            content: dict[str, Any] = load(f)

        return content

    except JSONDecodeError as e:
        print(f"{e.__class__.__name__}, {e.lineno + 1}:{e.colno + 1}\n{e.msg}")
        raise

    except TOMLDecodeError as e:
        print(f"{e.__class__.__name__}, {e.line + 1}:{e.col + 1}\n{str(e)}")
        raise

    except YAMLError as e:
        if isinstance(e, MarkedYAMLError):
            _: str = f"{e.__class__.__name__}, {e.problem}, {e.problem_mark}\n{e.note}"

        else:
            _: str = f"{e.__class__.__name__}, {str(e)}"

        print(_)
        raise

    except FileNotFoundError as e:
        print(f"{e.__class__.__name__}, file {e.filename} is not found, ")
        raise

    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        raise


def write_file_content(file: str | Path, content: Mapping[str, Any]):
    try:
        with open(file, "w", encoding="utf-8", errors="ignore") as f:
            match file.suffix:
                case ".json":
                    from json import dump

                    kwargs: dict[str, int | bool] = {
                        "ensure_ascii": False,
                        "indent": 2,
                        "sort_keys": True
                    }

                case ".toml":
                    from tomlkit import dump

                    kwargs: dict[str, int | bool] = {
                        "sort_keys": True
                    }

                case ".yaml":
                    from yaml import safe_dump as dump

                    kwargs: dict[str, int | bool] = {
                        "indent": 2,
                        "sort_keys": True
                    }

                case _:
                    raise ValueError

            dump(content, f, **kwargs)

        print(f"File {file} is successfully recorded")

    except ConvertError as e:
        print(f"{e.__class__.__name__}, {str(e)}")
        raise

    except YAMLError as e:
        if isinstance(e, MarkedYAMLError):
            _: str = f"{e.__class__.__name__}, {e.problem}, {e.problem_mark}\n{e.note}"

        else:
            _: str = f"{e.__class__.__name__}, {str(e)}"

        print(_)
        raise

    except OSError as e:
        print(f"{e.__class__.__name__}, {e.strerror}")
        raise
