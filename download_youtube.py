# -*- coding: utf-8 -*-
from http.client import HTTPException
from typing import Mapping
from urllib.error import HTTPError

from pytube import Stream, YouTube
import os.path as p
from pydub.audio_segment import AudioSegment


# URL: str = "https://www.youtube.com/watch?v=ijLTUW4HljM"
# MIME_TYPE: str = "audio/mp4"
# ABR: str = "128kbps"
# OUTPUT_PATH: str = p.expanduser("~/Desktop")
# FILENAME: str = "lis_music.m4a"
# START: int = 416_000
# STOP: int = 490_000
# OUT_F: str = p.expanduser("~/Desktop/lis_final.m4a")
# FORMAT: str = "mp4"
# BITRATE: str = "128k"
# CODEC: str = "aac"


def generate_proxies(proxy_ip: str, proxy_port: int, username: str, password: str):
    return {
        "http": f"http://{username}:{password}@{proxy_ip}:{proxy_port}",
        "https": f"https://{username}:{password}@{proxy_ip}:{proxy_port}"
    }


def download(
        url: str, *,
        mime_type: str,
        abr: str,
        output_path: str,
        filename: str,
        only_audio: bool = True,
        proxies: Mapping[str, str] = None):
    if proxies is None:
        proxies: dict[str, str] = dict()

    else:
        proxies: dict[str, str] = {**proxies}

    try:
        yt: YouTube = YouTube(url, proxies=proxies)
        stream: Stream = yt.streams.filter(
            mime_type=mime_type,
            only_audio=only_audio,
            abr=abr).first()
        file: str = stream.download(output_path=output_path, filename=filename)

        if p.exists(file) and p.getsize(file):
            print("Success!")

        else:
            print("Failed")

    except HTTPError as e:
        print(
            f"{e.__class__.__name__}\nОшибка {e.strerror}\nПричина {e.reason}\nЗапрос {e.geturl()}\nКод {e.getcode()}")

    except HTTPException as e:
        print(f"{e.__class__.__name__}\nОшибка b'{str(e)}'")

    except OSError as e:
        print(f"{e.__class__.__name__}\nОшибка {e.strerror}")


def process_file(file: str, *, start: int, stop: int, out_f: str, format_: str, bitrate: str, codec: str):
    audio: AudioSegment = AudioSegment.from_file(p.expanduser(file))
    segment: AudioSegment = audio[start:stop]
    try:
        segment.export(
            out_f=out_f,
            format=format_,
            codec=codec,
            bitrate=bitrate)

    except Exception as e:
        print(f"{e.__class__.__name__}, {e.args}")
        print("Piece is failed to be generated")

    else:
        print("Piece is successfully generated")


if __name__ == '__main__':
    URL: str = "https://www.youtube.com/watch?v=SNhyVTg5MfA"
    MIME_TYPE: str = "audio/mp4"
    ABR: str = "128kbps"
    OUTPUT_PATH: str = p.expanduser("~/Desktop")
    FILENAME: str = "Resonance - Soul Eater Opening.m4a"
    ONLY_AUDIO: bool = True

    PROXY_IP: str = "171.22.127.38"
    PROXY_PORT: int = 46799
    USERNAME: str = "proxy_user"
    PASSWORD: str = "EIgFlUZJFMLUd9LI"

    PROXIES: dict[str, str] = generate_proxies(PROXY_IP, PROXY_PORT, USERNAME, PASSWORD)

    print(PROXIES)

    download(
        URL,
        mime_type=MIME_TYPE,
        abr=ABR,
        output_path=OUTPUT_PATH,
        filename=FILENAME,
        only_audio=ONLY_AUDIO,
        proxies=PROXIES)
