from collections import Counter
from typing import Iterable

initial_data: str = """\
TS;04.03.2024;05.03.2024;13.03.2024;14.03.2024;22.03.2024;;;01.02.2024;05.02.2024;07.02.2024;08.02.2024;12.02.2024;14.02.2024;19.02.2024;20.02.2024;21.02.2024;22.02.2024;27.02.2024;;;09.01.2024;11.01.2024;;;;;;;
MME;01.03.2024;04.03.2024;07.03.2024;20.03.2024;21.03.2024;;;05.02.2024;09.02.2024;15.02.2024;16.02.2024;19.02.2024;26.02.2024;27.02.2024;29.02.2024;;;;;;16.01.2024;;;;;;;;
Globus_OSS;11.03.2024;12.03.2024;18.03.2024;21.03.2024;22.03.2024;;;;;;;;;;;;;;;;;;;;;;;;
SGW;06.03.2024;15.03.2024;18.03.2024;22.03.2024;;;;20.02.2024;;;;;;;;;;;;;16.01.2024;;;;;;;;
Globus_PASS;05.03.2024;06.03.2024;11.03.2024;12.03.2024;15.03.2024;19.03.2024;21.03.2024;01.02.2024;14.02.2024;15.02.2024;19.02.2024;22.02.2024;27.02.2024;28.02.2024;29.02.2024;;;;;;30.01.2024;;;;;;;;
Basics;13.04.2024;14.04.2024;;;;;;;;;;;;;;;;;;;;;;;;;;;
SigFW;13.04.2024;;;;;;;12.02.2024;15.02.2024;19.02.2024;21.02.2024;27.02.2024;;;;;;;;;19.01.2024;22.01.2024;24.01.2024;;;;;;
SCEF;04.03.2024;06.03.2024;11.03.2024;12.03.2024;;;;28.02.2024;;;;;;;;;;;;;;;;;;;;;
Style_guide;01.03.2024;04.03.2024;06.03.2024;18.03.2024;22.03.2024;25.03.2024;26.03.2024;01.02.2024;02.02.2024;05.02.2024;06.02.2024;07.02.2024;08.02.2024;09.02.2024;27.02.2024;;;;;;12.01.2024;15.01.2024;16.01.2024;;;;;;
link_repair;07.03.2024;10.03.2024;11.03.2024;;;;;05.02.2024;07.02.2024;08.02.2024;;;;;;;;;;;11.01.2024;;;;;;;;
docx_to_markdown;14.03.2024;;;;;;;11.02.2024;12.02.2024;13.02.2024;14.02.2024;15.02.2024;;;;;;;;;;;;;;;;;
docx_modify;;;;;;;;05.02.2024;06.02.2024;07.02.2024;08.02.2024;09.02.2024;12.02.2024;16.02.2024;19.02.2024;20.02.2024;21.02.2024;22.02.2024;26.02.2024;27.02.2024;09.01.2024;10.01.2024;11.01.2024;12.01.2024;14.01.2024;;;;
document_builder;;;;;;;;01.02.2024;;;;;;;;;;;;;29.01.2024;;;;;;;;
SGSN;;;;;;;;05.02.2024;;;;;;;;;;;;;;;;;;;;;
RG;;;;;;;;06.02.2024;14.02.2024;;;;;;;;;;;;;;;;;;;;
SMSFW;;;;;;;;07.02.2024;;;;;;;;;;;;;;;;;;;;;
SG;;;;;;;;07.02.2024;;;;;;;;;;;;;;;;;;;;;
DocsTemplate;;;;;;;;07.02.2024;;;;;;;;;;;;;25.01.2024;;;;;;;;
OSS;;;;;;;;08.02.2024;;;;;;;;;;;;;;;;;;;;;
SB;;;;;;;;09.02.2024;;;;;;;;;;;;;;;;;;;;;
Antifraud;;;;;;;;13.02.2024;;;;;;;;;;;;;;;;;;;;;
PGW;;;;;;;;16.02.2024;;;;;;;;;;;;;25.01.2024;;;;;;;;
Shared;;;;;;;;19.02.2024;22.02.2024;;;;;;;;;;;;;;;;;;;;
STP;;;;;;;;;;;;;;;;;;;;;29.01.2024;31.01.2024;;;;;;;
"""


class ProjectWithDates:
    def __init__(
            self,
            project_name: str,
            dates: Iterable[str] = None,
            spent_time: int = 0):
        if dates is None:
            dates: list[str] = []
        self.project_name: str = project_name
        self.dates: list[str] = [*dates]
        self.spent_time: int = spent_time

    def __str__(self):
        return f"{self.project_name}: {self.spent_time}"

    def month_dates(self, month: int = None):
        if month is not None:
            return list(filter(lambda x: x.removesuffix(".2024").endswith(f"{month}"), self.dates))
        else:
            return self.dates


class ProjectDict(dict):
    def __init__(self, projects: Iterable[ProjectWithDates] = None):
        if projects is None:
            projects: dict[str, ProjectWithDates] = dict()
        else:
            projects: dict[str, ProjectWithDates] = {project.project_name: project for project in projects}
        super().__init__(**projects)

    def __add__(self, other):
        if isinstance(other, ProjectWithDates):
            self[other.project_name] = other
        else:
            return NotImplemented

    def find_projects(self, project_date: str):
        return list(filter(lambda x: project_date in x.dates, self.values()))

    def counter(self, month: int = None):
        project: ProjectWithDates
        times: list[str] = list(
            project_date for project in self.values() for project_date in project.month_dates(month))
        return Counter(times)

    def distribute_time(self, month: int = None):
        for project_date, count in self.counter(month).items():
            addition: int = 480 // count

            for project in self.find_projects(project_date):
                project.spent_time += addition

        return

    def nullify(self):
        for project in iter(self.values()):
            project.spent_time = 0

    def total_sum(self):
        return sum(project.spent_time for project in self.values())


project_dict: ProjectDict = ProjectDict()


def parse():
    for line in initial_data.splitlines():
        project_name: str = line.split(";", 1)[0]
        dates: list[str] = list(filter(lambda x: x != "", line.removeprefix(f"{project_name};").split(";")))
        project_with_dates: ProjectWithDates = ProjectWithDates(project_name, dates)
        project_dict + project_with_dates


if __name__ == '__main__':
    print("------------------------------")
    print("Full quarter")

    parse()
    project_dict.distribute_time()

    print(f"\nSum:\n\n{project_dict.total_sum()}\n")

    for v in project_dict.values():
        print(str(v))

    print("------------------------------")
    print("January")

    project_dict.nullify()
    project_dict.distribute_time(1)

    for v in project_dict.values():
        print(str(v))

    print(f"\nSum:\n\n{project_dict.total_sum()}\n")

    print("------------------------------")
    print("February")

    project_dict.nullify()
    project_dict.distribute_time(2)

    for v in project_dict.values():
        print(str(v))

    print(f"\nSum:\n\n{project_dict.total_sum()}\n")

    print("------------------------------")
    print("March")

    project_dict.nullify()
    project_dict.distribute_time(3)

    for v in project_dict.values():
        print(str(v))

    print(f"\nSum:\n\n{project_dict.total_sum()}\n")

    print("------------------------------")
