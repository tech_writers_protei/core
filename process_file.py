from collections import deque
from pathlib import Path
from typing import Iterable

_attributes: list[str] = [
    '\n',
    ":asciidoctorconfigdir: ../../../\n",
    ":source-highlighter: highlightjs\n",
    ":table-caption: Таблица\n\n",
    'ifeval::["{backend}" != "pdf"]\n',
    ':spacex1: &nbsp;&nbsp;\n',
    ':spacex2: &nbsp;&nbsp;&nbsp;&nbsp;\n',
    ':spacex3: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\n',
    'endif::[]\n',
    '\n',
    'ifeval::["{backend}" == "pdf"]\n',
    ':spacex1: {empty}\n',
    ':spacex2: {empty}\n',
    ':spacex3: {empty}\n',
    'endif::[]\n\n'
]

def generate_front_matter(title: str, description: str = "") -> list[str]:
    return [
        '---\n',
        f'title: "{title}"\n',
        f'description: "{description}"\n',
        'weight: 30\n',
        'type: docs\n',
        '---\n'
    ]


class AdocFile:
    def __init__(self, path: str | Path):
        self._path: Path = Path(path).resolve()
        self._content: deque[str] = deque()
        self._title: str | None = None
        self._description: str | None = None

    def __iter__(self):
        return iter(self._content)

    def __getitem__(self, item):
        return self._content[item]

    def read(self):
        with open(self._path, "r", encoding="cp1251") as f:
            _: list[str] = f.readlines()
        self._content = deque(_)

    def __add__(self, other):
        if isinstance(other, str):
            self._content.appendleft(other)
        elif isinstance(other, Iterable):
            self._content.extendleft(other)
        else:
            print(f"Invalid item {other} to add")

    # def __radd__(self, other):
    #     if isinstance(other, str):
    #         self._content.append(other)
    #     elif isinstance(other, Iterable):
    #         self._content.extend(other)
    #     else:
    #         print(f"Invalid item {other} to add")

    __radd__ = __add__

    def specify(self):
        self._description, self._title = self[1].removeprefix("== ").removesuffix("\n").split(", ")

    def process(self):
        reversed([*generate_front_matter(self._title, self._description), *_attributes]) + self

    def write(self):
        with open(self._path, "w") as f:
            f.write("".join(iter(self)))
        print(f"File {self._path.name} succeeded")


if __name__ == '__main__':
    _path: str = r"C:\Users\tarasov-a\PycharmProjects\Globus_OSS\content\common\oam\api"
    for file in Path(_path).iterdir():
        if file.stem.startswith("monitoring_"):
            adoc_file: AdocFile = AdocFile(file)
            adoc_file.read()
            adoc_file.specify()
            adoc_file.process()
            adoc_file.write()
