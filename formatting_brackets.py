from textwrap import dedent


def formatting_trigger(line: str):
    indent_level: int = 0
    dspaces: str = "  "

    _line: list[str] = [*line]

    for idx, char in enumerate(iter(_line)):
        if char not in ("(", ")"):
            continue

        if char == "(":
            indent_level += 1

        elif char == ")":
            indent_level -= 1

        _line[idx + 1] = f"\n{indent_level * dspaces}{_line[idx + 1]}"

    return "".join(_line)


if __name__ == '__main__':

    _string: str = dedent(""" \
"aliasSub(aliasByTags(movingMin(seriesByTag('Node={{ node }}', 'name=S1Peers.state', \
'app={{ app.name }}'), '1min'), 'TAC', 'PLMN', 'ip'), '^(\\d+)\\.(\\d+)\\.(\\d+.\\d+.\\d+.\\d+)$', 'IP=\\3, \
TAC=\\1,PLMN=\\2'){% if app.s1peer.alias_list is defined %}{% for alias in app.s1peer.alias_list %}| aliasSub('( \
.*{{ alias.link | replace(".", "\\\\.") }}$)', '\\1.{{ alias.alias }}'){% endfor %}{% endif %}" \
""")
    print(formatting_trigger(_string))
