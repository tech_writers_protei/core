from glob import iglob
from os import sep, rename
from os.path import splitext
from xml.etree.ElementTree import Element, parse, ElementTree, tostring, register_namespace

from cairosvg import svg2png


def change_color(image: str, color: str = None):
    register_namespace("", "http://www.w3.org/2000/svg")

    _xml_image: ElementTree = parse(image)
    _element: Element = _xml_image.getroot()

    if "stroke" not in _element.attrib:
        print(f"File {image} has no stroke element")
        return "NONE"
    else:
        old_color: str = _element.get('stroke')
        print(f"Old color: {old_color}")

        _element.set("stroke", f"{color}")
        _element.set("stroke-width", "1")

        with open(image, "wb") as fb:
            fb.write(tostring(_element, encoding="utf-8", xml_declaration=False))

        print(f"Color changed for file {image} to {color}")

    return old_color


def change_stroke_width(image: str, width: int):
    register_namespace("", "http://www.w3.org/2000/svg")

    _xml_image: ElementTree = parse(image)
    _element: Element = _xml_image.getroot()

    if "stroke-width" not in _element.attrib:
        print(f"File {image} has no stroke-width element")
    else:
        _element.set("stroke-width", f"{width}")

        with open(image, "wb") as fb:
            fb.write(tostring(_element, encoding="utf-8", xml_declaration=False))

        print(f"Stroke width changed for file {image} to {width}")


def convert(image: str, color: str):
    file_name: str = splitext(image)[0]
    outfile: str = f"{file_name}.png"

    old_color: str = change_color(image, color)

    svg2png(url=image, output_height=300, write_to=outfile)
    print(f"PNG file {outfile} is generated")

    change_color(image, old_color)


def rename_file(file: str, postfix: str):
    name, extension = splitext(file)
    _new_name: str = f"{name}_{postfix}{extension}"
    # _new_name: str = f"{name.removesuffix(postfix)}{extension}"
    rename(file, _new_name)


def convert_files():
    path: str = r"C:\Users\tarasov-a\Desktop\svg\light_blue"
    # _color: str = "#7D858F"
    _color: str = "#5D9AF4"

    for item in iglob("*.svg", root_dir=path):
        image_file: str = f"{path}{sep}{item}"
        convert(image_file, _color)
        # change_stroke_width(image_file, 1)


def rename_images():
    path: str = r"C:\Users\tarasov-a\Desktop\svg\light_blue"

    for item in iglob("*.png", root_dir=path):
        image_file: str = f"{path}{sep}{item}"
        rename_file(image_file, "light_blue")


if __name__ == '__main__':
    convert_files()
    rename_images()
