# -*- coding: utf-8 -*-
from decimal import Decimal
from glob import iglob
from os import sep
from re import compile, IGNORECASE, Match, Pattern, search
from typing import Mapping, NamedTuple

from loguru import logger
from PIL import Image

_pattern: Pattern = compile(r"image:([^\[]+)(?=\.png)\.png\[([^]]*)]", IGNORECASE)
_extensions: tuple[str, ...] = ("png", "jpg", "jpeg", "svg")
_default_height: int = 30


def parse(line: str):
    _dict: dict[str, str | int] = dict()

    for _ in line.split(","):
        k: str
        v: str
        k, v = _.split("=")
        if v.isnumeric():
            v: int = int(v)
        _dict[k] = v

    return _dict


def get_image_storage(basepath: str) -> dict[str, str]:
    _image_storage: dict[str, str] = dict()

    for item in iglob("**/*", root_dir=basepath, recursive=True):
        if item.rsplit(".", 1)[-1].lower() in _extensions:
            name: str = item.rsplit(sep, 1)[-1].rsplit(".", 1)[0]
            path: str = f"{basepath}{sep}{item}"
            _image_storage[name] = path

    return _image_storage


def resize_image(path: str, h: int) -> tuple[int, int]:
    if h == -1:
        h: int = _default_height

    logger.info(f"image = {path}")
    with Image.open(path) as image:
        image.load()

    width, height = image.size
    ratio: Decimal = Decimal(width / height).quantize(Decimal("1.0000"))
    return int(ratio * h), h


class InlineImageLink(NamedTuple):
    index: int
    image: str
    attributes: dict[str, str | int] = dict()

    def __add__(self, other):
        if not isinstance(other, Mapping):
            raise ValueError
        else:
            self.attributes.update({f"{k}": f"{v}" for k, v in other.items()})

    def has_attribute(self, attribute: str) -> bool:
        return attribute in self.attributes.keys()

    def __str__(self):
        attributes_string: str = ",".join(f"{k}={v}" for k, v in self.attributes.items())
        return f"image:{self.image}.png[{attributes_string}]"

    @classmethod
    def from_string(cls, index: int, line: str):
        _m: Match = search(_pattern, line)
        image: str = _m.group(1)
        attributes: dict[str, str | int] = parse(_m.group(2))

        return cls(index, image, attributes)

    def __setitem__(self, key, value):
        if isinstance(key, str) and isinstance(value, (str, int)):
            self.attributes[key] = value
        else:
            raise TypeError

    def __getitem__(self, item):
        if isinstance(item, str) and item in self.attributes:
            return self.attributes.get(item)
        else:
            raise KeyError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self._fields == other._fields
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self._fields != other._fields
        else:
            return NotImplemented


class AsciiDocFile(NamedTuple):
    image_storage: dict[str, str]
    path: str
    content: list[str] = []
    inline_image_links: dict[int, list[InlineImageLink]] = dict()

    def nullify(self):
        self.content.clear()
        self.inline_image_links.clear()

    def __str__(self):
        return f"{self.__class__.__name__}: {self.path}"

    def links(self) -> list[InlineImageLink]:
        return [link for index, links in self.inline_image_links.items() for link in links]

    def __iter__(self):
        return iter(self.content)

    def __add__(self, other):
        if isinstance(other, InlineImageLink):
            if other.index not in self.inline_image_links:
                self.inline_image_links[other.index] = []
            self.inline_image_links[other.index].append(other)
        else:
            raise ValueError

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.path == other.path
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return self.path != other.path
        else:
            return NotImplemented

    def read(self):
        try:
            with open(self.path, "r", encoding="utf8") as f:
                self.content.extend(f.readlines())
        except FileNotFoundError:
            logger.error(f"Файл {self.path} не найден")
            raise
        except RuntimeError:
            logger.error(f"Обработка файла {self.path} не успешна")
            raise
        except OSError as e:
            logger.error(
                f"Ошибка с файлом {self.path}: {self.__class__.__name__}\n{e.strerror}")
            raise

    def set_links(self):
        for index, line in enumerate(iter(self)):
            _start: int = line.find("image:")

            if _start == -1 or line[_start + 6] == ":":
                continue
            else:
                self + InlineImageLink.from_string(index, line)

    def write(self):
        try:
            with open(self.path, "w", encoding="utf8") as f:
                _lines: str = "".join(self.content)
                logger.info(_lines)
                f.write(_lines)
        except FileNotFoundError:
            logger.error(f"Файл {self.path} не найден")
            raise
        except RuntimeError:
            logger.error(f"Обработка файла {self.path} не успешна")
            raise
        except OSError as e:
            logger.error(
                f"Ошибка с файлом {self.path}: {self.__class__.__name__}\n{e.strerror}")
            raise
        else:
            logger.info(f"Файл {self.path} обновлен")

    def update_content(self, index: int, __old: str, __new: str):
        logger.info(f"__old = {__old}")
        logger.info(f"__new = {__new}")
        self.content[index] = self.content[index].replace(__old, __new)
        logger.info(f"content = {self.content}")

    def update_links(self):
        for inline_image_link in self.links():
            logger.info(f"inline_image_link = {str(inline_image_link)}")
            if inline_image_link.has_attribute("width"):
                continue

            __old: str = str(inline_image_link)

            path: str = self.image_storage.get(f"{inline_image_link.image}")

            logger.info(f"{inline_image_link.image}")

            if inline_image_link.has_attribute("height"):
                h: int = inline_image_link["height"]
            else:
                h: int = _default_height

            width, height = resize_image(path, h)

            inline_image_link["width"] = width
            inline_image_link["height"] = height

            logger.info(str(inline_image_link))

            __new: str = str(inline_image_link)

            self.update_content(inline_image_link.index, __old, __new)


def get_asciidoc_files(dirpath: str) -> tuple[str, ...]:
    return tuple(
        f"{dirpath}{sep}{file_path}"
        for file_path in iglob("**/*.adoc", root_dir=dirpath, recursive=True))


def parse_attributes(attributes: str, substring: str) -> int:
    if substring not in attributes:
        return -1

    for _sub in attributes.split(","):
        if not _sub.startswith(substring):
            continue
        else:
            return int(_sub.removeprefix(substring))


def main():
    _path: str = r"C:\Users\tarasov-a\PycharmProjects\Globus_PASS\content\common\web"
    _images: str = r"C:\Users\tarasov-a\PycharmProjects\Globus_PASS\content\common\web"

    _image_storage = get_image_storage(_images)
    logger.info(f"_image_storage = {_image_storage.items()}")

    for asciidoc_path in get_asciidoc_files(_path):
        ascii_doc_file: AsciiDocFile = AsciiDocFile(_image_storage, asciidoc_path)
        ascii_doc_file.read()
        ascii_doc_file.set_links()
        ascii_doc_file.update_links()

        logger.info(ascii_doc_file.inline_image_links)
        logger.info(ascii_doc_file.content)

        ascii_doc_file.write()
        ascii_doc_file.nullify()


if __name__ == '__main__':
    main()
